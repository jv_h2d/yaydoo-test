# Yaydoo 🙌
Hola Yaydoo, encantado de realizar este test con ustedes. Con este archivo deberían poder realizar las pruebas unitarias  y correr la simulación de los proyectos.

Primero quiero agradecer la oportunidad brindada por su parte, seguido de pedir una disculpa por los pocos commits, olvidé que tenía que ir commiteando cada cambio. Bueno, empecemos...

- Siguiendo las instrucciones del test, separé las dos clases para poder tener una mejor estructura (Productos, Seguros).
- Dentro de la clase Seguros, se tiene un switch para saber qué tipo de producto está recibiendo y con base a ello decidir qué función ejecutar.
- Se desarrollaron funciones específicas para 4 productos y para los otros dos una función default con un parámetro adicional (el ritmo en que difiere el precio con base a los días transcurridos).
- Se crearon un par de métodos para sumar o restar valor al precio dependiendo de las condiciones del producto ```restPrice()``` y ```addPrice()```.
---
## Instalación

Usar el sistema de gestión de paquetes [npm](https://www.npmjs.com/) para instalar los node_modules.

```bash
npm i
```
o
```bash
npm install
```

## Uso
Solo son necesarios dos comandos para su uso:
### Simulación de los 30 días
```bash
npm run after-30-days
```
y se obtendrá la simulación de los 30 días y la variación de cada producto.

### Pruebas unitarias
En este caso es necesario ejecutar el comando
```bash
npm test
```
Y con ayuda de los módulos chai y mocha se realizarán las pruebas configuradas en el archivo `~/test/testSpec.js` para más información consultar la documentación de [chai](https://www.chaijs.com/)

---

## Información adicional
- Desarrollado con node v14.17.3
- Chai versión 4.3.4
- Mocha versión 9.1.1
- Console table printer versión 2.10.0
## Autor
JJ ✌️