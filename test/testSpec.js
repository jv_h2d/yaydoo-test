let chai = require('chai');
let assert = chai.assert;
let should = chai.should();
let expect = chai.expect;

const Insur = require('../src/CarInsurance');
const Prod = require('../src/Product');

const arr = [
    new Prod('Medium Coverage', 10, 20)
]
const price = new Insur( [ arr ] );
const test1 = [ { name: 'Medium Coverage', sellIn: 9, price: 19 } ]


describe('Testing assert function: ', function() {
    describe('Check updatePrice Function', function(){
        it('Check the returned value using : assert.equal(value, value): ', function() {
            const products = price.updatePrice();
            /*Uncomment for test name*/
            // expect(products[0].name, test1[0].name);

            /*Uncomment for test sellIn*/
            // expect(products[0].sellIn, test1[0].sellIn);
            
            /*Uncomment for test price*/
            // expect(products[0].price, test1[0].price);
        });
    });
})