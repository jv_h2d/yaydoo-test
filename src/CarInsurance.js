class CarInsurance {
  constructor(products = []) {
      this.products = products;
  }

  /* Método restPrice obtiene el precio y lo que se debe de restar, 
   * esto para simplificar la lectura del código cuando este se utiliza.
   */
  restPrice(price, subs){
    return ( price - subs ) > 0 ? ( price - subs ) : 0 ;
  }

  /* Método addPrice obtiene el precio y lo que se debe de sumar, 
   * esto para simplificar la lectura del código cuando este se utiliza.
   */
  addPrice(price, plus){
    let total = price += plus;
    return total < 50 ? total : 50 ;
  }

  /* Método defaultPrice configura el comportamiento de los precios de cada producto,
   * el parámetro decrement funciona por el producto Super Sale, como éste se decrementa 
   * el doble de rápido, se añade éste parámetro extra para no escribir una función adicional.
   */
  defaultPrice(prod, decrement){
    prod.sellIn -= 1;
    if(prod.sellIn >= 0){
      if(prod.price > 0){
        prod.price = this.restPrice(prod.price, (1*decrement) )
      }else {
        prod.price = 0;
      }
    }else {
      if(prod.price > 0){
        prod.price = this.restPrice(prod.price, (2*decrement) )
      }else {
        prod.price = 0;
      }
    }
    return {name: prod.name, sellIn: prod.sellIn, price: prod.price};
  }
  
  /* Método fcPrice configura el comportamiento de los precios del producto
   * Full Coverage Price a través de los días.
   */
  fcPrice(prod){
    prod.sellIn = prod.sellIn - 1;
    if(prod.sellIn >= 0){
      if(prod.price < 50){
        prod.price += 1;
      }else {
        prod.price = 50;
      }
    }else {
      if(prod.price < 50){
        prod.price += 2;
      }else {
        prod.price = 50;
      }
    }
    return {name: prod.name, sellIn: prod.sellIn, price: prod.price};
  }

  /* Método sfcPrice configura el comportamiento de los precios del producto
   * Special Full Coverage Price a través de los días, cumpliendo con las condiciones
   * establecidas.
   */
  sfcPrice(prod){
    prod.sellIn = prod.sellIn - 1;
    if(prod.sellIn >= 10){
      if(prod.price < 50){
        prod.price = this.addPrice(prod.price, 1)
      }else {
        prod.price = 50;
      }
    }else if (prod.sellIn >= 5){
      if(prod.price < 50){
        prod.price = this.addPrice(prod.price, 2)
      }else {
        prod.price = 50;
      }
    }else if(prod.sellIn >= 0 ){
      if(prod.price < 50){
        prod.price = this.addPrice(prod.price, 3)
      }else {
        prod.price = 50;
      }
    } else {
      prod.price = 0;
    }
    return {name: prod.name, sellIn: prod.sellIn, price: prod.price};
  }

  /* Método updatePrice que con ayuda de un enum (por buenas prácticas) compara el nombre
   * del Producto envíado y con base a la entrada procesa la salida
   */
  updatePrice() {
    let P = {
      FulCov: "Full Coverage",
      MegCov: "Mega Coverage",
      SpeFulCov: "Special Full Coverage",
      SupSale: "Super Sale"
    }
    let prod = []
    for (var i = 0; i < this.products.length; i++) {
      let op = this.products[i].name;
      switch (op) {
        case P.FulCov:
          prod.push(this.fcPrice(this.products[i]));
          break;
        case P.MegCov:
          prod.push(this.products[i]);
          break;
        case P.SpeFulCov:
          prod.push(this.sfcPrice(this.products[i]));
          break;
        case P.SupSale:
          prod.push(this.defaultPrice(this.products[i], 2));
          break;
      
        default:
          prod.push(this.defaultPrice(this.products[i], 1))
          break;
      }
    }
    return prod;
  }
}

module.exports = CarInsurance
