const { printTable } = require('console-table-printer');
const Product = require("./src/Product");
const CarInsurance = require("./src/CarInsurance");


/* Arreglo con los productos y su configuración inicial 
 * Con la que se hará la simulación de los 30 días 
 */
const productsAtDayZero = [
    new Product('Medium Coverage', 10, 20),
    new Product('Full Coverage', 2, 0),
    new Product('Low Coverage', 5, 8),
    new Product('Mega Coverage', 0, 80),
    new Product('Mega Coverage', -1, 80),
    new Product('Special Full Coverage', 15, 20),
    new Product('Special Full Coverage', 10, 49),
    new Product('Special Full Coverage', 5, 49),
    new Product('Super Sale', 3, 3),
  ];

const prices = new CarInsurance(productsAtDayZero);

/* Ciclo for para simular los 30 días llamando el método updatePrice()
 * que reconoce qué producto se está enviando */

for(let i = 0; i<= 30; i++){
    console.log(`Day ${i}`);
    if(i== 0){
        let boot = []
        for(let j = 0; j < productsAtDayZero.length; j+=1){
            let item = productsAtDayZero[j];
            boot.push(item)
        }
        printTable(boot)
    }else {
        printTable(prices.updatePrice());
    }
    console.log('');
}